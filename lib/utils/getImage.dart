  ///Сопоставление картинок и иконок из api
  String getImage(int type){
  if(type == 1 || type == 2 || type == 3 || (type>=30 && type <= 34)){
    return 'sun.png';
  }
  if(type == 4|| type == 5 || type == 6 || type == 20 || type == 21 || type == 35 ||  type ==36){
    return 'cloudy-day.png';
  }
  if(type == 7 || type == 8 || type == 11 || type == 37 || type ==38){
    return 'clouds.png';
  }
  if(type >= 22 && type <= 25 || type == 29 || type ==44 ||  type == 19){
    return 'cloud-with-snowflakes.png';
  }
  if((type >= 12 && type <=14) || type == 26 || (type >=39 && type <= 40)){
    return 'sun-and-rain.png';
  }
  if(type == 18){
    return 'rain.png';
  }
  if(type == 15 ||type == 16 ||type == 17 || type ==41 || type == 42){
    return 'storm.png';
  }
  return 'sun.png';
}