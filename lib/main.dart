import 'package:flutter/material.dart';
import 'package:weather_app/pages/main_page.dart';


void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with SingleTickerProviderStateMixin{

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MainPage(),
      title: 'Weather',
      debugShowCheckedModeBanner: false,
    );
  }
}