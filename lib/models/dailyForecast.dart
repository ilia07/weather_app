class DailyForecast{
  Date date;
  Day day;
  Temperature temperature;
  DailyForecast({this.date,this.day, this.temperature});
  factory DailyForecast.fromJson(Map <String, dynamic> json){
    return DailyForecast(
      date: Date.fromStr(json['Date'].split('T')[0]),
      day: Day.fromJson(json['Day']),
      temperature: Temperature.fromJson(json['Temperature']),
    );
  }
}

class Date{
  String year;
  String month;
  String day;
  Date({this.day, this.month, this.year});
  factory Date.fromStr(str){
    str = str.split('-');
    return Date(
      day: str[2],
      month: str[1],
      year: str[0],
    );
  }
}

class Temperature{
  int averageValue;
  double min;
  double max;
  Temperature({this.max, this.min, this.averageValue});
  factory Temperature.fromJson(Map <String , dynamic> json){
    double min = TemperatureTile.fromJson(json['Minimum']).value;
    double max = TemperatureTile.fromJson(json['Maximum']).value;
    double averageValue = (min + max)/2; 
    return Temperature(
      averageValue: averageValue.round(),
      max: max,
      min: min,
    );
  }
}

class TemperatureTile{
  double value;
  TemperatureTile({this.value});
  factory TemperatureTile.fromJson(Map <String, dynamic> json){
    return TemperatureTile(
      value: (json['Value'] - 32) * 5/9,
    );
  }
}

class Day{
  int icon;
  String iconPhrase;
  String precipitationType;
  Day({this.icon,this.iconPhrase, this.precipitationType});
  factory Day.fromJson(Map <String, dynamic> json){
    return Day(
      icon: json['Icon'],
      iconPhrase: json['IconPhrase'],
      precipitationType: json['PrecipitationType'],
    );
  }
}