import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:weather_app/models/dailyForecast.dart';
import 'package:weather_app/utils/formaterDate.dart';
import 'package:http/http.dart' as http;
import 'package:weather_app/utils/getImage.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with SingleTickerProviderStateMixin{

  Color primaryColor;
  Color secondaryColor;

  List<DailyForecast> list;

  String apiKey = 'FUmX0Lb3JGm0E88wZFgy7oT9ZrRRybpA';
  String cityKey = '290868';
  
  Future <dynamic> getWeatherNext5Day() async {
    String url = 'http://dataservice.accuweather.com/forecasts/v1/daily/5day/$cityKey?apikey=$apiKey&language=ru';
    var response = await http.get(url);
    if(response.statusCode == 200){
      var strJson = jsonDecode(response.body);
      var tempList = strJson['DailyForecasts'] as List;
      list = tempList.map((i) => DailyForecast.fromJson(i)).toList();
      return list;
    }else{
      String error = json.decode(response.body)['Message'];
      throw Exception(error);
    }
  }

  void setBackgroundColor(int temperature){
    if(temperature <= 0){
      primaryColor = Color(0xff81C5D8);
      secondaryColor = Color(0xff39AECF);
    }else{
      primaryColor = Color(0xffF5D896);
      secondaryColor = Color(0xffFCB040);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.cyan,
        body: FutureBuilder(
          future: getWeatherNext5Day(),
          builder: (context, snaphsot){
            if(snaphsot.hasError){
              return Center(
                child: Text('${snaphsot.error}', style: TextStyle(fontSize: 16, color: Colors.white), textAlign: TextAlign.center,),
              );
            }
            if(snaphsot.connectionState == ConnectionState.none && snaphsot.hasData == null){
              return Center(
                child: Text('Отсутствует подключение к сети интернет'),
              );
            }
            if(snaphsot.connectionState == ConnectionState.waiting){
              return Center(
                child: CircularProgressIndicator(backgroundColor: Colors.transparent, valueColor: AlwaysStoppedAnimation<Color>(Colors.white),),
              );
            }
            if(snaphsot.hasData){
              setBackgroundColor(list[0].temperature.averageValue);
              return Stack(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    color: primaryColor,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 40, vertical: 60),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text('Томск', style: TextStyle(fontSize: 30, color: Colors.white),),
                                SizedBox(height: 5,),
                                Text(list[0].day.iconPhrase, style: TextStyle(fontSize: 24, color: Colors.white),)
                              ],
                            ),
                            Container(
                              height: 55,
                              width: 100,
                              child: Stack(
                                alignment: Alignment.bottomCenter,
                                children: <Widget>[
                                  Text(list[0].temperature.averageValue.toString(), style: TextStyle(color: Colors.white, fontSize: 56)),
                                  Positioned(
                                    top: 0,
                                    right: 0,
                                    child: Container(
                                      height: 12,
                                      width: 12,
                                      decoration: BoxDecoration(color: Colors.transparent, border:Border.all(width: 2, color: Colors.white), borderRadius: BorderRadius.circular(50)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 150,
                        width: 150,
                        child: Image.asset('assets/${getImage(list[0].day.icon)}', color: Colors.white,),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                        decoration: BoxDecoration(color: secondaryColor, borderRadius: BorderRadius.circular(50)),
                        child: Text(createStrDate(list[0].date), style: TextStyle(color: Colors.white, fontSize: 16),),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                        decoration: BoxDecoration(color: secondaryColor),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: List.generate(5, (int index) => 
                            Column(
                              children: <Widget>[
                                Text(list[index].date.day, style: TextStyle(fontSize: 16, color: Colors.white),), 
                                SizedBox(height: 15,),
                                Container(
                                  height: 20,
                                  width: 20,
                                  child: Image.asset('assets/${getImage(list[index].day.icon)}', color: Colors.white,),
                                ),
                                SizedBox(height: 15,),
                                Container(
                                  height: 20,
                                  width: 32,
                                  child: Stack(
                                    alignment: Alignment.bottomCenter,
                                    children: <Widget>[
                                      Text(list[index].temperature.averageValue.toString(), style: TextStyle(fontSize: 16, color: Colors.white),),
                                      Positioned(
                                        top: 0,
                                        right: 0,
                                        child: Container(
                                          height: 5,
                                          width: 5,
                                          decoration: BoxDecoration(color: Colors.transparent, border:Border.all(width: 1, color: Colors.white), borderRadius: BorderRadius.circular(50)),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )
                          )
                        ),
                      ),
                    ],
                  ),
                ],
              );
            }
          },
        ),
      ),
    );
  }
}